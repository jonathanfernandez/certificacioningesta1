@ActivateSegment
Feature: Feature for FdevJob

  Scenario: Test FdevJob should result OK
    Given a config file path in HDFS hdfs://hadoop:9000/FdevJob.conf
    When execute FdevJob in Spark
    Then should send metrics to Kafka with key FdevJob-test
    And should have 0 lines written and 0 lines readed

  Scenario Outline: Test custom implementations result OK
    Given a config file path in HDFS hdfs://hadoop:9000/<pathConfig>.conf
    When execute FdevJob in Spark
    Then should send metrics to Kafka with key FdevJob-test
    And should have <linesWritten> lines written and <linesRead> lines readed

      Examples:
        | pathConfig     | linesWritten | linesRead |
        | FdevJob | 0            | 0         |

